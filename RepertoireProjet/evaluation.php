<!-- exemple de description des modalités d'évaluation d'une UE -->
<!-- la classe NOTE attribue une couleur -->
<!-- la classe FORMULE pour les tags  -->

<p>
L'évaluation s'effectue sur la base de deux rendus intermédiaires (fin janvier, fin février) et 1 rendu final (début avril) du projet. Les rendus sont sur gitlab de l’étudiant et partagé avec l’enseignant. La note finale est pondérée avec la présence en TPS, exprimé par les « commit » dans gitlab et la feuille de présence. 

Trois notes seront attribuées à chaque étudiant durant le semestre :
</p>

<ul>


<li> <span class="NOTE">Projet_Etape_1</span> : une appréciation A/B/C/D/E du projet par l'enseignant intervenant dans le groupe en fin janvier.   </li> 

<li> <span class="NOTE">Projet_Etape_2</span> : une appréciation A/B/C/D/E du projet par l'enseignant intervenant dans le groupe en fin février.  </li> 

<li> <span class="NOTE">Projet_Etape_3</span> : une appréciation A/B/C/D/E du projet par l'enseignant intervenant dans le groupe en fin janvier.  </li> 
 
</ul>


<p>
La note finale sur 20 (<span class="NOTE">N</span>) est calculée comme une moyenne pondérée de ces notes. Les appréciations sont pondéréee en fonction des présences en projet. 
</p>

<p class="FORMULE">
 N = (1*Projet_Etape_1 + 2*Projet_Etape_2 + 2*Projet_Etape_3) /5
</p>
<P> Des points bonus sont prévus. </p>




<p>Pas de session de rattrapage prévu pour cette UE. </p>
 

<p>
L'unité acquise apporte 6 ECTS.
</p>

<!-- signature -->
<div class="signature">
   <!-- VOTRE NOM ICI --> <br/>
   dernière modification : 
<?php echo date(" d/m/Y à H:i:s", getlastmod()); ?>
</div>
