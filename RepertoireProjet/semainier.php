<div class="semainier">        


<table class="table">

<tr class="entete">
<th>  </th>
<th> Cours </th>
<th> TD </th>
<th> TP </th>
<th> Remarque </th>
</tr>



<!-- ========================================================= -->
<!-- SEANCE -->
<tr class="seance" >
<td>du 08/01 au 13/01</td>
<td> 
<!-- COURS -->
<a href="https://gitlab.univ-lille.fr/chabane.djeraba/projet-l3-s6/-/blob/main/Projet-L3-S2_v2.pdf"> Introduction du projet </a> 
</td>
<td> 
<!-- TD -->

</td>
<td > 
<!-- TP -->
  Structure de données et architecture du logiciel 
</td>
<td class="remarque"> 
<!-- REMARQUE -->
</td>
</tr>
<!-- FIN SEANCE -->
<!-- ========================================================= -->



<!-- ========================================================= -->
<!-- SEANCE -->
<tr class="seance" >
<td>du 15/01 au 20/01</td>
<td> 
<!-- COURS -->

</td>
<td> 
<!-- TD -->

</td>
<td> 
<!-- TP -->
Algorithme de Recherche
</td>
<td class="remarque"> 
<!-- REMARQUE -->
</td>
</tr>
<!-- FIN SEANCE -->
<!-- ========================================================= -->


<!-- ========================================================= -->
<!-- SEANCE -->
<tr class="seance" >
<td>du 22/01 au 27/01</td>
<td> 
<!-- COURS -->
Introduction du projet
</td>
<td> 
<!-- TD -->

</td>
<td> 
<!-- TP -->
Programme de Recherche
</td>
<td class="remarque"> 
<!-- REMARQUE -->

</td>
</tr>
<!-- FIN SEANCE -->
<!-- ========================================================= -->



<!-- ========================================================= -->
<!-- SEANCE -->
<tr class="seance" >
<td>du 29/01 au 03/02</td>
<td> 
<!-- COURS -->
Introduction du projet

</td>
<td> 
<!-- TD -->

</td>
<td>
<!-- TP -->
Algorithme d'insertion 
</td>
<td class="remarque"> 
<!-- REMARQUE -->
</td>
</tr>
<!-- FIN SEANCE -->
<!-- ========================================================= -->


<!-- ========================================================= -->
<tr class="seance" >
<!-- FIN SEANCE -->
<td>du 05/02 au 10/02</td>
<td>
<!-- COURS -->

</td>
<td> 
<!-- TD -->

</td>
<td>
<!-- TP -->
  Programme d'insertion
</td>
<td class="remarque"> 
<!-- REMARQUE -->
</td>
</tr>
<!-- FIN SEANCE -->
<!-- ========================================================= -->


<!-- ========================================================= -->
<tr class="seance" >
<td>du 12/02 au 17/02</td>
<td>
</td>
<td> 
</td>
<td>
<!-- TP -->
Programme d'insertion
</td>
<td class="remarque">
<!-- REMARQUE -->

</td>
</tr>
<!-- ========================================================= -->



<!-- ========================================================= -->
<!-- FIN SEANCE -->
<tr class="seance" >
<td>du 19/02 au 24/02</td>
<td>
<!-- COURS -->

</td>
<td>
<!-- TD -->

</td>
<td>
<!-- TP -->
Algorithme de suppression
</td>
<td class="remarque"> 

</td>
</tr>
<!-- FIN SEANCE -->
<!-- ========================================================= -->





<!-- ========================================================= -->
<!-- SEANCE -->
<tr class="libre" >
<td>du 26/02 au 02/03</td>
<td> 
<!-- COURS -->

</td>
<td> 
<!-- TD -->

</td>
<td> 
<!-- TP -->

  
</td>
<td class="remarque"> 
<!-- REMARQUE -->
interruption pédagogique hiver
</td>
</tr>
<!-- FIN SEANCE -->
<!-- ========================================================= -->

<!-- ========================================================= -->
<!-- SEANCE -->
<tr class="seance" >
<td>du 04/03 au 09/03</td>
<td> 
<!-- COURS -->

</td>
<td> 
<!-- TD -->

</td>
<td>
<!-- TP -->
Programme de suppression
</td>
<td class="remarque"> 
<!-- REMARQUE -->

</td>
</tr>
<!-- FIN SEANCE -->
<!-- ========================================================= -->


<!-- ========================================================= -->
<!-- SEANCE -->
<tr class="seance" >
<td>du 11/03 au 16/03</td>
<td> 
<!-- COURS -->

</td>
<td> 
<!-- TD -->

</td>
<td> 
<!-- TP -->
Programme de suppression
  
</td>
<td class="remarque"> 
<!-- REMARQUE -->

</td>
</tr>
<!-- FIN SEANCE -->
<!-- ========================================================= -->

<!-- ========================================================= -->
<!-- SEANCE -->
<tr class="seance" >
<td>du 18/03 au 23/03</td>
<td> 
<!-- COURS -->

</td>
<td> 
<!-- TD -->

</td>
<td> 
<!-- TP -->
Protocole d'expérimentation et bonus
</td>
<td class="remarque"> 
<!-- REMARQUE -->
</td>
</tr>
<!-- FIN SEANCE -->
<!-- ========================================================= -->



<!-- ========================================================= -->
<tr class="seance" >
<td>du 25/03 au 30/03</td>
<td> <!-- COURS -->

</td>
<td> 
<!-- TD -->

</td>
<td> 
<!-- TP -->
Protocole d'expérimentation et bonus
</td>
<td class="remarque"> 
<!-- REMARQUE -->
</td>
</tr>
<!-- ========================================================= -->


<!-- ========================================================= -->
<!-- SEANCE -->
<tr class="seance" >
<td>du 01/04 au 06/04</td>
<td> 
<!-- COURS -->

</td>
<td> 
<!-- TD -->

</td>
<td> 
<!-- TP -->
 Evaluation des projets 
</td>
<td class="remarque"> 
<!-- REMARQUE -->
lundi 1er avril férié
</td>
</tr>
<!-- FIN SEANCE -->
<!-- ========================================================= -->



</table>



</div>



<!-- signature -->
<div class="signature">
   <!-- VOTRE NOM ICI --> <br/>
   dernière modification : 
<?php echo date(" d/m/Y à H:i:s", getlastmod()); ?>
</div>
