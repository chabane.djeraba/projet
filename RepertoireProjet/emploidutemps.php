<div class="edt">     

<a href="https://www.fil.univ-lille.fr/~aubert/l3/intervenantL3S6/"> Les emplois du temps </a> 
  
 <p> </p>
 <p> </p>
  
<table class="table">
<tr class="entete">
<th> Gpe </th>
<th> Nature </th>
<th> Horaire </th>
<th> Salle </th>
<th> Enseignant </th>
<th> e-mail </th>
</tr>

<!-- ------------------------ EXEMPLE --------------------- -->
<!-- <tr class="TYPE" > avec TYPE = Cours | CTD | TD | TP   -->
<!-- <td> 2</td>              -->
<!-- <td> td </td>              -->
<!-- <td> Mercredi 15h45-16h45</td>         -->
<!-- <td> M5 - A3</td>              -->
<!-- <td> Machin Chose</td>           -->
<!-- <td>               -->
<!-- <a href="mailto:Machin.Chose@univ-lille.fr?subject=[MonUE]"> -->
<!-- Machin.Chose@univ-lille.fr </a>            -->
<!-- </td>                            -->
<!-- </tr>                                                  -->
<!-- --------------------- FIN EXEMPLE -------------------- -->

<!-- COURS -->
<tr class="cours" >                        
<td> <!-- GROUPE  -->          </td>
<td> <!-- NATURE  -->   Cours   

  Projet

  
  </td>
<td> <!-- HORAIRE -->         
  <li> mardi 9 janvier, 13 h - 14 h 30 </li> 
<li> lundi 22 janvier, 16 h 30 – 18 h </li> 
<li> lundi 29 janvier 16 h 30 – 18 h </li> 
   </td>
<td> <!-- SALLE   -->         amphi BACCHUS, M5 </td>
<td> <!-- ENSEIGNANT -->    Chaabane Djeraba   </td>
<td> <!-- MAIL -->       
<a href="mailto:chabane.djeraba@univ-lille.fr"> chaabane Djeraba</a>
</td>
</tr>
<!-- ============================================================ -->

<!-- ============================================================ -->
<!-- TEMPLATE A COPIER -->
<!-- ============================================================ -->
<!-- TD -->
<tr class="TD" >                        
<td> <!-- GROUPE  -->          </td>
<td> <!-- NATURE  -->    TD      </td>
<td> <!-- HORAIRE -->          </td>
<td> <!-- SALLE   -->          </td>
<td> <!-- ENSEIGNANT -->       </td>
<td> <!-- MAIL -->       
<!-- <a href="mailto:@univ-lille.fr?subject=[ ]"> @univ-lille.fr</a> -->
</td>
</tr>
<!-- ============================================================ -->
<!-- TP -->
</tr><tr class="TP" >                        
<td> <!-- GROUPE  -->      G1    </td>
<td> <!-- NATURE  -->    TP      </td>
<td> <!-- HORAIRE -->    jeudi matin, 8H-11h30       </td>
<td> <!-- SALLE   -->     M5-A12     </td>
<td> <!-- ENSEIGNANT -->   Salman Farhat    </td>
<td> <!-- MAIL -->  
  
  <a href="mailto:salam.farhat@univ-lille.fr"> Salam Farhat </a>
      
<!-- <a href="mailto:@univ-lille.fr?subject=[ ]"> @univ-lille.fr</a> -->
</td>
</tr>
  
  <!-- TP -->
</tr><tr class="TP" >                        
<td> <!-- GROUPE  -->      G2    </td>
<td> <!-- NATURE  -->    TP      </td>
<td> <!-- HORAIRE -->    mardi après-midi, 14H45-18h15       </td>
<td> <!-- SALLE   -->     M5-A11     </td>
<td> <!-- ENSEIGNANT -->   Chaabane Djeraba    </td>
<td> <!-- MAIL -->     <a href="mailto:chabane.djeraba@univ-lille.fr"> chaabane Djeraba</a>    
<!-- <a href="mailto:@univ-lille.fr?subject=[ ]"> @univ-lille.fr</a> -->
</td>
</tr>

  
  <!-- TP -->
</tr><tr class="TP" >                        
<td> <!-- GROUPE  -->      G3    </td>
<td> <!-- NATURE  -->    TP      </td>
<td> <!-- HORAIRE -->    mercredi matin, 8H-11h30       </td>
<td> <!-- SALLE   -->     M5-A11     </td>
<td> <!-- ENSEIGNANT -->   Chaabane Djeraba    </td>
<td> <!-- MAIL -->     
<a href="mailto:chabane.djeraba@univ-lille.fr  "> Chaabane Djeraba</a>
</td>
</tr>

<!-- TP -->
</tr><tr class="TP" >                        
<td> <!-- GROUPE  -->      G4    </td>
<td> <!-- NATURE  -->    TP      </td>
<td> <!-- HORAIRE -->    mercredi matin, 8H-11h30       </td>
<td> <!-- SALLE   -->     M5-A12     </td>
<td> <!-- ENSEIGNANT -->   Damien Pollet    </td>
<td> <!-- MAIL -->     <a href="mailto:damien.pollet@univ-lille.fr"> Damien Pollet</a>    
<!-- <a href="mailto:@univ-lille.fr?subject=[ ]"> @univ-lille.fr</a> -->
</td>
</tr>

<!-- TP -->
</tr><tr class="TP" >                        
<td> <!-- GROUPE  -->      G5    </td>
<td> <!-- NATURE  -->    TP      </td>
<td> <!-- HORAIRE -->    jeudi matin, 8H-11h30       </td>
<td> <!-- SALLE   -->     M5-A11     </td>
<td> <!-- ENSEIGNANT -->   Pierre Allegraud    </td>
<td> <!-- MAIL -->   
 <a href="mailto:pierre.allegraud@univ-lille.fr"> Pierre Allegraud</a>
  
</td>
</tr>

<!-- TP -->
</tr><tr class="TP" >                        
<td> <!-- GROUPE  -->      G6    </td>
<td> <!-- NATURE  -->    TP      </td>
<td> <!-- HORAIRE -->    jeudi matin, 8H-11h30       </td>
<td> <!-- SALLE   -->     M5-A13     </td>
<td> <!-- ENSEIGNANT -->   Jean-Luc Intumwayase    </td>
<td> <!-- MAIL -->       
<a href="mailto:jeanluc.intumwayase@univ-lille.fr "> Jean Luc Intumwayase</a>
</td>
</tr>
<!-- ============================================================ -->
<!-- FIN TEMPLATE -->
<!-- ============================================================ -->

</table>

</div><!-- edt -->

<!-- signature -->
<div class="signature">
   <!-- VOTRE NOM ICI --> <br/>
   dernière modification : 
<?php echo date(" d/m/Y à H:i:s", getlastmod()); ?>
</div>

