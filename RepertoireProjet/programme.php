<!-- objectifs de l'UE -->
<h3> Objectifs </h3>
Objectif de l’UE est de dépasser les limites des TPs en réalisant un projet avec une structure de données avancée (ex. arbre de recherche n-aire et équilibré), la programmation python à objets et l’algorithmique avancée. Le tout avec un processus de conception et de réalisation d'un projet orienté objet. 
L'objectif aussi est de donner aux étudiants la possibilité de réaliser un projet de leur choix sous réserve d'inclure les structures de données avancées, l'algorithmique avancée et python orienté objet.  

<!-- le contenu -->
<h3> Contenu </h3>
Le contenu est divisé en 4 grandes parties. La première partie réalise les structures de données et la méthode de recherche. La deuxième partie réalise la méthode d'insertion. La troisième partie réalise la méthiode de suppression. Et la quatrième partie réalise les protocoles de tests. 

<!-- bibliographie accompagnant l'UE -->
<h3> Bibliographie </h3>
Bayer, R.; McCreight, E.M. (1972), "Organization and maintenance of large ordered indexes", Acta Informatica,1(3): 173 - 189, doi : 10.1007/bf00288683.



<!-- signature -->
<div class="signature">
   <!-- VOTRE NOM ICI --> <br/>
   dernière modification : 
<?php echo date(" d/m/Y à H:i:s", getlastmod()); ?>
</div>
