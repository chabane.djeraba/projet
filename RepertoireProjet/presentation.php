<!-- Le nom de l'UE -->
<div class="UE">PROJET</div>
<div class="bcc">BCC 4 : Mise en oeuvre des langages de programmation</div>

<h3> Pré-requis </h3>
Les pré-requis pour cette UE&#xA0;:
<li> UML </li>
<li> Processus génie logiciel de réalisation d'un projet </li>
<li> Programmation objet via Python </li>
<li> algorithmique et structure de données avancés </li>

<!-- Volume horaire et organisation de l'UE C, CTD, TD, TP -->
<h3> Organisation  </h3>

<p>
Cette unité se déroule au S6 de la licence mention Informatique.
Volume horaire : 3x1h30 de cours, 12x3h30 de TP

</p>
<p>
Volume horaire : 1h30 de cours, 1h30 de TD et 1h30 de TP par semaine, pendant 12 semaines.</p>

<p>
Cette UE constitue un pré-requis  de le stage.
</p>



<h3> Crédits </h3>

<strong>6 ECTS</strong>


<!-- le ou les responsable(s) -->
<h3> Responsable </h3>

<p>
<a href="mailto:CHABANE.DJERABA@univ-lille.fr?subject[UE]">Chaabane Djeraba</a>
</p>

<h3> Intervenants </h3>

<li> Chaabane Djeraba </li>
<li>   Salman Farhat  </li>
<li>   Damien Pollet      </li>
<li>   Pierre Allegraud   </li>
<li>  Jean-Luc Intumwayase </li>  


<!-- signature -->
<div class="signature">
   <!-- VOTRE NOM ICI --> <br/>
   dernière modification : 
<?php echo date(" d/m/Y à H:i:s", getlastmod()); ?>
</div>
